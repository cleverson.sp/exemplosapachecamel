package com.apacheCamel.Exemplo2;

import org.apache.camel.CamelContext;
import org.apache.camel.Route;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TesteController {

	@Autowired
	private CamelContext contexto;

	@GetMapping("/iniciar-backup")
	public ResponseEntity iniciarBackup() throws Exception {
		Route rota = contexto.getRoute("rota-backup");

		if (rota == null) {
			contexto.addRoutes(new BackupArquivosRoute());
		} else {
			rota.getConsumer().start();
		}
		return ResponseEntity.ok().build();
	}
	
	@GetMapping("/parar-backup")
	public ResponseEntity pararBackup() throws Exception {
		
		Route rota = contexto.getRoute("rota-backup");
		if(rota!=null) {
			rota.getConsumer().stop();
		}
		System.out.println("Parouuuuu !");
		return ResponseEntity.ok().build();
	}

}
