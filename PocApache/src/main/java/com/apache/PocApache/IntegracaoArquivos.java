package com.apache.PocApache;

import org.apache.camel.CamelContext;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.impl.DefaultCamelContext;

public class IntegracaoArquivos {

	public static void main(String[] args) throws Exception {
		CamelContext contexto = new DefaultCamelContext();
		contexto.addRoutes(new RouteBuilder() {

			@Override
			public void configure() throws Exception {

				errorHandler(defaultErrorHandler().maximumRedeliveries(2).redeliveryDelay(2_000)
						.onExceptionOccurred(new Processor() {

							@Override
							public void process(Exchange exchange) throws Exception {
								System.out.println("Deu ruim na copia ! \n");
								System.out.println(
										"tentativa" + exchange.getMessage().getHeader(Exchange.REDELIVERY_COUNTER));
								System.out.println("Erro \n" + exchange.getException());
							}
						}));

				from("file://origem/?recursive=true&noop=true&antInclude=*.txt").process(new Processor() {
					@Override
					public void process(Exchange exchange) throws Exception {
						String nomeArquivo = exchange.getIn().getHeader(Exchange.FILE_NAME, String.class);

						System.out.printf("\nTentando copiar o arquivo %s \n", nomeArquivo);

						exchange.getIn().setHeader(Exchange.FILE_NAME, nomeArquivo + ".bk");
					}
				})

						.delay(5_000).to("file://destino/").process(new Processor() {
							@Override
							public void process(Exchange exchange) throws Exception {
								String nomeArquivo = exchange.getOut().getMessageId();
								System.out.printf("\nCópia o arquivo concluido %s \n", nomeArquivo);
							}
						})

						.end();
			}
		});
		contexto.start();

		for (;;) {
		}
	}
}
